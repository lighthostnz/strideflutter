# Infrastructure and tooling

## Table of content

- [1. Non-Functional Requirement](#1-non-functional-requirement)
- [2. Security](#2-security)
- [3. Testing](#3-testing)
- [4. Linter](#4-linter)
- [5. Flavors](#5-flavors)
- [6. Logger](#6-logger)
- [7. SonarQube](#7-sonarqube)

## 1. Non-Functional Requirement

- Non Functional requirement defines “how System supposed to do”.

a non-functional requirement is a requirement that specifies criteria that can be used to judge the operation of a system, rather than specific behaviors.

non-functional requirements—can be divided into two main categories:

1. Execution qualities, such as safety, security and usability, which are observable during operation (at run time).
2. Evolution qualities, such as testability, maintainability, extensibility and scalability, which are embodied in the static structure of the system.

Moving on let me List out few Key types of NFR thats needs to be taken care of:

1.  Performance

    - The performance of the Application can be determined by it responsive time, time to complete the given task. Make sure while using the app, app should not lagging and experience should be smooth.

    How we can make flutter performance?

    - Generally, Flutter applications are performance by default, so you only need to avoid common pitfalls to get excellent performance. These best recommendations will help you write the most performance Flutter app possible. but still there are some practices. let's take a look into it:

      - Controlling build() cost:

      - We should avoid call build method frequently because it's impact to the performance of the app.

      - Avoid overly large single Widgets with a large build() function because if we call setState function then it's called the whole build method and it will be rebuild.

      - Use `const` constructor whenever possible because `const` will be replaced with its value during compilation.

      - Render grids and lists lazily.

      [More info here](https://flutter.dev/docs/perf/rendering/best-practices)

2.  Scalability

    - App should able to adopt it self to increased usage or able to handle more data as time progress. BLoC architecture is meant for scalable/large apps.

      - Scalable code is well-tested.

      - Avoid custom code whenever possible.

      - Select and stick with one state management solution.

      - Write clear documentation.

      - Ensure consistency.

      - Business logic and UI should be separate.

      [More info here](https://verygood.ventures/blog/scalable-best-practices)

3.  Reliability

    - The application should be reliable to perform the business.

    - This quality attribute specifies how likely the system or its element would run without a failure for a given period of time under predefined conditions.

      - For example :- when user perform some important action it should be acknowledged with confirmation. We should consider the feedback given by users and try to solve that issues.

    - Make sure that we are showing the time depends on timezone in our app.

4.  Availability

    - There should be a common plane where the user can access your application to install and look for regular updates give feedback.

      - For example :- Apples’s App Store and Googles Play Store

5.  Localization

    - Does the system match local specifics?

[More info here](https://medium.com/@vishwasng/non-functional-requirement-of-the-mobile-development-system-e0ed98f2a872)

## 2. Security

App security isn’t a feature or a benefit – it is a bare necessity. Security has always been a major concern for businesses. And this concern is even greater when it comes to mobile apps.

But according to a [survey](https://www.pixelcrayons.com/blog/mobile-app-stats/?utm_source=freecodecamp&utm_medium=mobile%2Bapp%2Bdevelopment_sk&utm_campaign=website), more than 75% of mobile applications will fail basic security tests.

### Encrypt API key

flutter_secure_storage plugin can be used to store the api key in encrypted format. This plugin is inspired by `libsecret` and `Keychain`.

```dart
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

// Create storage
final storage = new FlutterSecureStorage();

// Read value
String value = await storage.read(key: key);

// Read all values
Map<String, String> allValues = await storage.readAll();

// Delete value
await storage.delete(key: key);

// Delete all
await storage.deleteAll();

// Write value
await storage.write(key: key, value: value);
```

[Plugin link](https://pub.dev/packages/flutter_secure_storage)

### SSL Certificate Pinning

SSL Pinning prevents MITM (Man in the Middle Attack). To put simply, when you connect to a public WIFI or hotspot, the IT guy that takes care of the network, either good or bad, can get the traffic from your mobile devices to the server that you connect to. For more details, you can go to look them up on professional websites like one below. The bottom line is don’t use public WIFI or anyone else’s hotspot!!!

We just need to tell the app that NOT to trust any certificates except the ones that you provide in the mobile app.

Main purpose of using SSL-pinning is to prevent your flutter app from hijack attack, and keep your app secure from hackers.

Security is another big component that we empathize with users. They trust us by providing us with their valuable private information, so it’s our honor and responsibility to equip our system with complete protections from insiders and outsiders. By insiders, we mean developers like us. For data privacy reasons, we don’t have the ability to look into our database or see user’s information at all.

If you’re looking into Stackoverflow about SSL Pinning in Flutter or Dart, you might find a solution about [badCertificateCallback](https://api.flutter.dev/flutter/dart-io/HttpClient/badCertificateCallback.html).

[More info here](https://medium.com/kbtg-life/mobile-security-via-flutter-ep-1-ssl-pinning-c57f18b711f6)

### Pen-testing, malware analysis and security assessment

#### Mobile Security Framework (MobSF)

Mobile Security Framework (MobSF) is an automated, all-in-one mobile application (Android/iOS/Windows) pen-testing, malware analysis and security assessment framework capable of performing static and dynamic analysis. MobSF support mobile app binaries (APK, XAPK, IPA & APPX) along with zipped source code and provides REST APIs for seamless integration with your CI/CD or DevSecOps pipeline.The Dynamic Analyzer helps you to perform runtime security assessment and interactive instrumented testing.

The Mobile Security Framework (MobSF) is an open source framework capable of performing end to end security testing of mobile applications. MobSF can be used for security analysis of Android and iOS applications.

It support android and iOS.

- Static Analysis - Android

<img src="images/static_analysis_android.gif" alt="drawing" width="400"/>

- Static Analysis - iOS

<img src="images/static_analysis_iOS.gif" alt="drawing" width="400"/>

[More info here](https://mobsf.github.io/docs/#/)

#### FRIDA

Dynamic instrumentation toolkit for developers, reverse-engineers, and security researchers. it Works on Windows, macOS, GNU/Linux, iOS, Android, and QNX.

Using Python and JS allows for quick development with a risk-free API. Frida can help you easily catch errors in JS and provide you an exception rather than crashing.

We have to use python and js here. It support android and iOS.

### App security by flutter team

- Keep current with the latest Flutter SDK releases

- Keep your application’s dependencies up to date

- Keep your copy of Flutter up to date

[More info here](https://flutter.dev/security)

## 3. Testing

### Unit testing

Unit tests are handy for verifying the behavior of a single function, method, or class. The test package provides the core framework for writing unit tests, and the flutter_test package provides additional utilities for testing widgets.

Unit testing is a software testing method by which individual units of source code—sets of one or more computer program modules together with associated control data, usage procedures, and operating procedures—are tested to determine whether they are fit for use.

The goal of unit testing is to isolate each part of the program and show that the individual parts are correct. A unit test provides a strict, written contract that the piece of code must satisfy. As a result, it affords several benefits.

```dart
// Import the test package and Counter class
import 'package:test/test.dart';
import 'package:counter_app/counter.dart';

void main() {
  test('Counter value should be incremented', () {
    final counter = Counter();

    counter.increment();

    expect(counter.value, 1);
  });
}
```

[More info here](https://flutter.dev/docs/cookbook/testing/unit/introduction)

#### bloc_test

A Dart package that makes testing blocs and cubits easy. Built to work with bloc and mocktail.

blocTest creates a new bloc-specific test case with the given description. blocTest will handle asserting that the bloc emits the expected states (in order) after act is executed. blocTest also handles ensuring that no additional states are emitted by closing the bloc stream before evaluating the `expect`action.

```dart
group('CounterBloc', () {
  blocTest(
    'emits [] when nothing is added',
    build: () => CounterBloc(),
    expect: () => [],
  );

  blocTest(
    'emits [1] when CounterEvent.increment is added',
    build: () => CounterBloc(),
    act: (bloc) => bloc.add(CounterEvent.increment),
    expect: () => [1],
  );
});
```

[More info here](https://pub.dev/packages/bloc_test)

### Widget testing

Widget testing is otherwise called component testing. As its name proposes, it is utilized for testing a single widget, and the objective of this test is to check whether the widget works and looks true to form.

These test a single widget.

```dart
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  // Define a test. The TestWidgets function also provides a WidgetTester
  // to work with. The WidgetTester allows building and interacting
  // with widgets in the test environment.
  testWidgets('MyWidget has a title and message', (WidgetTester tester) async {
    // Create the widget by telling the tester to build it.
    await tester.pumpWidget(const MyWidget(title: 'T', message: 'M'));

    // Create the Finders.
    final titleFinder = find.text('T');
    final messageFinder = find.text('M');

    // Use the `findsOneWidget` matcher provided by flutter_test to
    // verify that the Text widgets appear exactly once in the widget tree.
    expect(titleFinder, findsOneWidget);
    expect(messageFinder, findsOneWidget);
  });
}

class MyWidget extends StatelessWidget {
  const MyWidget({
    Key? key,
    required this.title,
    required this.message,
  }) : super(key: key);

  final String title;
  final String message;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: Scaffold(
        appBar: AppBar(
          title: Text(title),
        ),
        body: Center(
          child: Text(message),
        ),
      ),
    );
  }
}
```

[More info here](https://flutter.dev/docs/testing#widget-tests)

### Integration test

An integration test tests a complete app or a large part of an app. The goal of an integration test is to verify that all the widgets and services being tested work together as expected. Furthermore, you can use integration tests to verify your app’s performance.

Generally, an integration test runs on a real device or an OS emulator, such as iOS Simulator or Android Emulator. The app under test is typically isolated from the test driver code to avoid skewing the results.

```dart
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  testWidgets("failing test example", (WidgetTester tester) async {
    expect(2 + 2, equals(5));
  });
}
```

[More info here](https://flutter.dev/docs/testing/integration-tests)

### Comparison

|                  | Unit  | Widget | Integration |
| ---------------- | ----- | ------ | ----------- |
| Confidence       | Low   | Higher | Highest     |
| Maintenance cost | Low   | Higher | Highest     |
| Dependencies     | Few   | More   | Most        |
| Execution speed  | Quick | Quick  | Slow        |

<img src="images/testing-pyramid.001.jpeg" alt="drawing" width="300"/>

## 4. Linter

Lint is a hand-picked, open-source, community-driven collection of lint rules for Dart and Flutter projects. The set of rules follows the [Effective Dart: Style Guide](https://dart.dev/guides/language/effective-dart/style).

Lint is customizable, you can add or remove rules, depending on your needs. To do so adjust your `analysis_options.yaml`.

```dart
include: package:lint/analysis_options.yaml

linter:
  rules:
    always_put_required_named_parameters_first: false
```

[More info here](https://pub.dev/packages/lint)

## 5. Flavors

We can pass the flavors using “--dart-define” flags while running the app. It was mainly used to enable Skia for the Flutter Web. But now we can pass multiple custom arguments.

For example

```dart
flutter run --dart-define=SOME_VAR=SOME_VALUE --dart-define=OTHER_VAR=OTHER_VALUE
```

Now we can use this values via “String.fromEnvironment(“SOME_VAR”)”

[More info here](https://itnext.io/flutter-1-17-no-more-flavors-no-more-ios-schemas-command-argument-that-solves-everything-8b145ed4285d)

## 6. Logger

Small, easy to use and extensible logger which prints beautiful logs. A Logger object is used to log messages for a specific system or application component.

How to integrate Logger in our app?

```dart
var logger = Logger();

logger.d("Logger is working!");
```

<img src="images/log_console_light.png" alt="drawing" width="300"/>

You can log with different levels:

```dart
logger.v("Verbose log");

logger.d("Debug log");

logger.i("Info log");

logger.w("Warning log");

logger.e("Error log");

logger.wtf("What a terrible failure log");
```

[More info here](https://pub.dev/packages/logger)

## 7. SonarQube

SonarQube empowers all developers to write cleaner and safer code.

SonarQube® is an automatic code review tool to detect bugs, vulnerabilities, and code smells in your code. It can integrate with your existing workflow to enable continuous code inspection across your project branches and pull requests.

There is not any official support for flutter from SonarQube organization but there is one third party plugin which can help us [sonar-flutter](https://github.com/insideapp-oss/sonar-flutter).

