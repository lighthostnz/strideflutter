import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:go_router/go_router.dart';
import 'package:stride/infrastructure/bloc/register/register_bloc.dart';
import 'package:stride/presentation/screens/auth/verify_phone/widget/otp_field_widget.dart';
import 'package:stride/presentation/widgets/rounded_button.dart';

import '../../../../application/constants/const.dart';
import '../../../../application/constants/strings.dart';

/// Widget for verify phone
class Verifyphone extends StatelessWidget {
  Verifyphone({Key? key, this.phoneNum}) : super(key: key);

  final String? phoneNum;
  final TextEditingController otp = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BlocListener<RegisterBloc, RegisterState>(
      listener: (context, state) {
        if (state is RegisterSuccess) {
          Center(
            child: Container(
              height: 184.h,
              width: 327.w,
              decoration: BoxDecoration(
                color: Const.kWhite,
                borderRadius: BorderRadius.circular(16.r),
              ),
            ),
          );
        }
      },
      child: Scaffold(
        backgroundColor: Const.backgroundcolorcyon,
        body: Padding(
          padding: EdgeInsets.only(left: 25.w, right: 25.w),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(
                  top: 58.h,
                ),
                child: GestureDetector(
                  onTap: () {
                    context.pop();
                  },
                  child: Icon(
                    Icons.arrow_back_ios,
                    color: Const.kWhite,
                    size: 18.h.w,
                  ),
                ),
              ),
              SizedBox(
                height: 34.h,
              ),
              Center(
                child: Text(
                  Strings.verify,
                  style:
                      Const.bold.copyWith(color: Const.kWhite, fontSize: 32.sp),
                ),
              ),
              SizedBox(
                height: 8.h,
              ),
              RichWidget(
                text: Strings.verifynotice,
                style:
                    Const.medium.copyWith(color: Const.kWhite, fontSize: 16.sp),
                span: [
                  TextSpan(
                    text: phoneNum,
                    style: Const.bold.copyWith(color: Const.kWhite),
                  )
                ],
              ),
              SizedBox(
                height: 27.h,
              ),
              Center(
                child: Otpfieldwidget(
                  onchange: (val) {},
                  otpController: otp,
                ),
              ),
              const Spacer(),
              Center(
                child: TextButton(
                  onPressed: () {},
                  child: Text(
                    Strings.resend,
                    style: Const.medium
                        .copyWith(color: Const.kWhite, fontSize: 16.sp),
                  ),
                ),
              ),
              SizedBox(
                height: 16.h,
              ),
              RoundedButton(
                color: Const.kLightBlackButton,
                onTap: () {
                  BlocProvider.of<RegisterBloc>(context)
                      .add(const FetchRegister(isRegister: true));
                },
                text: Strings.cont,
                arrow: false,
              ),
              SizedBox(
                height: 30.h,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
