import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

import '../../../../../application/constants/const.dart';
import '../../../../../application/constants/strings.dart';

/// Widget for OTP field
class Otpfieldwidget extends StatelessWidget {
  const Otpfieldwidget({Key? key, required this.onchange, this.otpController})
      : super(key: key);

  final TextEditingController? otpController;
  final void Function(String) onchange;

  @override
  Widget build(BuildContext context) {
    return PinCodeTextField(
      length: 4,
      autoFocus: true,
      obscureText: false,
      hapticFeedbackTypes: HapticFeedbackTypes.vibrate,
      useHapticFeedback: true,
      autovalidateMode: AutovalidateMode.disabled,
      textStyle: Const.medium.copyWith(fontSize: 16.sp, color: Const.kBlack),
      validator: (value) {
        final RegExp regExp = RegExp(r'(?<!\d)\d{6}(?!\d)');
        if (value!.isEmpty) {
          return "Please enter otp";
        } else if (!regExp.hasMatch(otpController!.text)) {
          return Strings.otpnotverify;
        }
        return null;
      },
      errorTextSpace: 25.h,
      mainAxisAlignment: MainAxisAlignment.center,
      animationType: AnimationType.fade,
      pinTheme: PinTheme(
        shape: PinCodeFieldShape.circle,
        borderRadius: BorderRadius.circular(5),
        fieldHeight: 50.h,
        fieldOuterPadding: EdgeInsets.only(left: 16.w),
        fieldWidth: 48.w.h,
        borderWidth: 1,
        activeColor: Const.kWhite,
        activeFillColor: Colors.white,
        selectedFillColor: Const.kWhite,
        disabledColor: Const.kWhite,
        errorBorderColor: Colors.white,
        selectedColor: Const.kBlack.withOpacity(0.5),
        inactiveFillColor: Const.kWhite,
        inactiveColor: Const.kWhite,
      ),
      animationDuration: const Duration(milliseconds: 300),
      backgroundColor: Const.backgroundcolorcyon,
      enableActiveFill: true,
      keyboardType: TextInputType.number,
      enablePinAutofill: true,
      showCursor: false,
      controller: otpController,
      onCompleted: (v) {},
      onChanged: onchange,
      beforeTextPaste: (text) {
        return true;
      },
      appContext: context,
    );
  }
}
