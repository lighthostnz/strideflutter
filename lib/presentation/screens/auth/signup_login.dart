import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:go_router/go_router.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:stride/presentation/router/app_router.dart';
import 'package:stride/presentation/widgets/rounded_button.dart';

import '../../../application/constants/const.dart';
import '../../../application/constants/strings.dart';

/// Widget for signup screen
class SignupLogin extends StatefulWidget {
  const SignupLogin({Key? key, this.isLogin}) : super(key: key);

  final bool? isLogin;

  @override
  State<SignupLogin> createState() => _SignupLoginState();
}

class _SignupLoginState extends State<SignupLogin> {
  final TextEditingController controller = TextEditingController();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final PhoneNumber number = PhoneNumber(isoCode: 'GB');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Const.backgroundcolorcyon,
      body: Padding(
        padding: EdgeInsets.only(left: 25.w, right: 25.w),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(
                top: 58.h,
              ),
              child: GestureDetector(
                onTap: () {
                  context.pop();
                },
                child: Icon(
                  Icons.arrow_back_ios,
                  color: Const.kWhite,
                  size: 18.h.w,
                ),
              ),
            ),
            SizedBox(
              height: 34.h,
            ),
            Text(widget.isLogin! ? Strings.welcomelogin : Strings.started,
                style:
                    Const.bold.copyWith(color: Const.kWhite, fontSize: 32.sp)),
            SizedBox(
              height: 8.h,
            ),
            Text(widget.isLogin! ? Strings.loginAccount : Strings.number,
                style: Const.medium
                    .copyWith(color: Const.lightsky, fontSize: 16.sp)),
            SizedBox(
              height: 27.h,
            ),
            Form(
              key: formKey,
              child: Container(
                width: 325.w,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8.r),
                  color: Const.kSemiLightsky,
                ),
                child: InternationalPhoneNumberInput(
                  inputDecoration: InputDecoration(
                    contentPadding: EdgeInsets.zero,
                    border: InputBorder.none,
                    errorBorder: InputBorder.none,
                    errorStyle: Const.medium
                        .copyWith(color: Const.kBlack, fontSize: 16.sp),
                  ),
                  maxLength: 10,
                  onInputChanged: (PhoneNumber number) {},
                  onInputValidated: (bool value) {},
                  validator: (val) {
                    RegExp regExp = RegExp(r'(^(?:[+0]9)?[0-9]{10,12}$)');
                    if (val!.isEmpty) {
                      return " Please enter phone number";
                    } else if (!regExp.hasMatch(controller.text)) {
                      return ' Please enter valid phone number';
                    }
                    return null;
                  },
                  textStyle: Const.medium
                      .copyWith(color: Const.kBlack, fontSize: 16.sp),
                  selectorConfig: const SelectorConfig(
                    trailingSpace: false,
                    leadingPadding: 10,
                    setSelectorButtonAsPrefixIcon: true,
                    showFlags: true,
                    selectorType: PhoneInputSelectorType.BOTTOM_SHEET,
                  ),
                  selectorButtonOnErrorPadding: 10,
                  ignoreBlank: false,
                  autoValidateMode: AutovalidateMode.disabled,
                  selectorTextStyle: Const.medium
                      .copyWith(color: Const.kBlack, fontSize: 16.sp),
                  initialValue: number,
                  textFieldController: controller,
                  formatInput: false,
                  keyboardType: const TextInputType.numberWithOptions(
                      signed: true, decimal: true),
                  inputBorder: InputBorder.none,
                  onSaved: (PhoneNumber number) {},
                ),
              ),
            ),
            SizedBox(
              height: 15.h,
            ),
            Text(
              widget.isLogin! ? Strings.loginnotice : Strings.noticesignup,
              style:
                  Const.medium.copyWith(color: Const.lightsky, fontSize: 12.sp),
            ),
            const Spacer(),
            RoundedButton(
              onTap: () {
                if (formKey.currentState!.validate()) {
                  context.push(AppRouter.verify, extra: controller.text);
                } else {}
              },
              color: Const.kBlack,
              arrow: widget.isLogin! ? false : true,
              text: widget.isLogin! ? Strings.login : Strings.verifynym,
            ),
            SizedBox(
              height: 30.h,
            ),
          ],
        ),
      ),
    );
  }
}
