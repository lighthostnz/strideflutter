import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:go_router/go_router.dart';
import 'package:stride/presentation/router/app_router.dart';
import 'package:stride/presentation/widgets/rounded_button.dart';

import '../../../application/constants/const.dart';
import '../../../application/constants/strings.dart';
import '../../../gen/assets.gen.dart';

/// Widget for welcome screen
class Welcome extends StatelessWidget {
  const Welcome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Const.backgroundcolorcyon,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(
              left: Const.kPaddingL.w, right: Const.kPaddingL.w),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 96.h),
              Assets.logo.logo
                  .svg(width: 150.w, height: 35.h, fit: BoxFit.cover),
              SizedBox(height: 40.h),
              Text(
                Strings.welcome,
                overflow: TextOverflow.ellipsis,
                maxLines: 4,
                softWrap: true,
                style: Const.large.copyWith(
                    fontSize: 72.sp,
                    fontWeight: FontWeight.w900,
                    color: Const.kWhite),
              ),
              SizedBox(height: 100.h),
              RichWidget(
                text: Strings.haveAccount,
                style:
                    Const.medium.copyWith(fontSize: 16.sp, color: Const.kWhite),
                span: [
                  TextSpan(
                    recognizer: TapGestureRecognizer()
                      ..onTap = () => context.push(AppRouter.auth, extra: true),
                    text: 'Log in',
                    style: Const.bold.copyWith(
                      color: Const.kWhite,
                    ),
                  )
                ],
              ),
              SizedBox(height: 15.h),
              RoundedButton(
                onTap: () => context.push(AppRouter.auth, extra: false),
                text: Strings.start,
                arrow: true,
                color: Const.kBlack,
              )
            ],
          ),
        ),
      ),
    );
  }
}
