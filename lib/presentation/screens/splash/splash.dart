import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:go_router/go_router.dart';
import 'package:stride/presentation/router/app_router.dart';

import '../../../application/constants/const.dart';
import '../../../gen/assets.gen.dart';

/// Widget for splash screen
class Splash extends StatefulWidget {
  const Splash({Key? key}) : super(key: key);

  @override
  State<Splash> createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(seconds: 4)).then((value) {
      Router.neglect(context, () {
        context.go(
          AppRouter.welcome,
        );
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Const.backgroundcolorcyon,
      body: Center(
        child: Center(
          child: Assets.logo.logo.svg(
            width: 200.w,
            height: 46.h,
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}
