import 'package:go_router/go_router.dart';
import 'package:stride/presentation/screens/welcome_screen/welcome_screen.dart';

import '../screens/auth/signup_login.dart';
import '../screens/auth/verify_phone/verify_otp.dart';
import '../screens/splash/splash.dart';

class AppRouter {
  static const String splash = 'splashScreen';
  static const String welcome = '/welcomeScreen';
  static const String auth = '/signupAndLogin';
  static const String verify = '/verifyScreen';

  const AppRouter._();

  static final router = GoRouter(
    routes: [
      GoRoute(
        path: '/',
        builder: (context, state) => const Splash(),
      ),
      GoRoute(
        path: welcome,
        builder: (context, state) => const Welcome(),
      ),
      GoRoute(
        path: auth,
        builder: (context, state) => SignupLogin(isLogin: state.extra as bool),
      ),
      GoRoute(
        path: verify,
        builder: (context, state) =>
            Verifyphone(phoneNum: state.extra as String),
      ),
    ],
  );
}
