import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../application/constants/const.dart';

/// Widget for rounded button
class RoundedButton extends StatelessWidget {
  const RoundedButton({
    Key? key,
    this.arrow = false,
    required this.color,
    required this.onTap,
    required this.text,
  }) : super(key: key);

  final Color? color;
  final String? text;
  final bool arrow;
  final void Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        height: 48.h,
        width: 325.w,
        decoration: BoxDecoration(
            color: color, borderRadius: BorderRadius.circular(48.r)),
        child: Row(
          children: [
            const Spacer(),
            Text(
              text ?? '',
              style:
                  Const.medium.copyWith(color: Const.kWhite, fontSize: 16.sp),
            ),
            const Spacer(),
            arrow
                ? Icon(
                    Icons.arrow_forward_rounded,
                    size: 28.h.w,
                    color: Const.kWhite,
                  )
                : const SizedBox.shrink(),
            SizedBox(
              width: arrow ? 21.w : 0.w,
            )
          ],
        ),
      ),
    );
  }
}
