part of 'register_bloc.dart';

/// All events of Registration
abstract class RegisterEvent extends Equatable {
  const RegisterEvent();
}

/// Event when user tries to verify the phone number
class FetchRegister extends RegisterEvent {
  const FetchRegister({this.isRegister});

  final bool? isRegister;

  @override
  List<Object?> get props => [isRegister];
}
