class Strings {
  /// Welcome
  static const welcome = 'Exercise is better with friends';
  static const haveAccount = 'Have an account? ';
  static const start = 'Get Started';

  /// Login&signUp
  static const started = 'Let\'s get started';
  static const number = 'Enter your phone number';
  static const otpnotverify = 'Otp can not be empty';
  static const noticesignup =
      'You will receive an SMS verification that may apply\nmessage and data rates. By continuing, you agree to our\nTerms of Service and Privacy Policy.';
  static const verifynym = 'Verify phone number';
  static const welcomelogin = 'Welcome back.';
  static const loginAccount = 'Log in to your account';
  static const login = 'Log in';
  static const loginnotice =
      'You will receive an SMS verification that may apply\nmessage and data rates.';
  static const verify = 'Verify phone';
  static const verifynotice =
      'Enter the 4-digit that we have sent via the\nphone number ';
  static const resend = 'Resend Code';
  static const cont = 'Continue';
}
