import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:logger/logger.dart';

import '../../gen/fonts.gen.dart';

class Const {
  /// UI Color
  static Color backgroundcolorcyon = const Color(0xff009B9B);
  static Color backgroundcolorlightcyon = const Color(0xff15BABA);

  /// Text Color
  static Color lightsky = const Color(0xffC1EDED);
  static Color kWhite = Colors.white;
  static Color kSemiLightsky = const Color(0xfff7f9fa);
  static Color kBlack = const Color(0xff090A0A);
  static Color kLightBlack = const Color(0xAA979C9E);

  /// ButtonColor
  static Color kLightskyButton = const Color(0xFFF2F4F5);
  static Color kLightBlackButton = const Color(0xFFE3E5E5);
  static Color kDarkskyButton = const Color(0xFF006C6C);

  /// Padding
  static const double kPaddingS = 8.0;
  static const double kPaddingM = 16.0;
  static const double kPaddingL = 25.0;
  static const double kPaddingXL = 32.0;

  /// Spacing
  static double kSpaceS = 8.w.h;
  static double kSpaceM = 25.w.h;

  /// Large
  static TextStyle large = TextStyle(
      color: Const.kBlack,
      fontWeight: FontWeight.bold,
      fontSize: 30.sp,
      fontFamily: FontFamily.inter);

  /// Medium
  static TextStyle bold = TextStyle(
      color: Const.kBlack,
      fontWeight: FontWeight.bold,
      fontSize: 16.sp,
      fontFamily: FontFamily.inter);

  /// Small
  static TextStyle medium = TextStyle(
      color: Const.kWhite,
      fontSize: 12.sp,
      fontWeight: FontWeight.normal,
      fontFamily: FontFamily.inter);

  void showError(String error, context) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(error),
        backgroundColor: Theme.of(context).errorColor,
      ),
    );
  }

  void showMessage(String message, context) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(message),
        backgroundColor: Theme.of(context).indicatorColor,
      ),
    );
  }

  static var logger = Logger(
    filter: null,
    printer: PrettyPrinter(),
    output: null,
  );
}

class RichWidget extends StatelessWidget {
  const RichWidget({Key? key, this.text, this.style, this.span})
      : super(key: key);

  final TextStyle? style;
  final String? text;
  final List<TextSpan>? span;

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(text: text, style: style, children: span),
    );
  }
}
