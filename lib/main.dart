import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:stride/infrastructure/bloc/register/register_bloc.dart';
import 'package:stride/infrastructure/debug/app_bloc_observer.dart';

import 'application/constants/const.dart';
import 'presentation/router/app_router.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Bloc.observer = AppBlocObserver();

  runApp(const StrideApp());
}

class StrideApp extends StatefulWidget {
  const StrideApp({Key? key}) : super(key: key);

  @override
  _StrideAppState createState() => _StrideAppState();
}

class _StrideAppState extends State<StrideApp> with WidgetsBindingObserver {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return OrientationBuilder(
          builder: (context, orientation) {
            return ScreenUtilInit(
              designSize: const Size(375, 812),
              builder: () {
                return BlocProvider<RegisterBloc>(
                  create: (context) => RegisterBloc(),
                  child: MaterialApp.router(
                    debugShowCheckedModeBanner: false,
                    theme: ThemeData(errorColor: Const.kWhite),
                    routeInformationParser:
                        AppRouter.router.routeInformationParser,
                    routerDelegate: AppRouter.router.routerDelegate,
                  ),
                );
              },
            );
          },
        );
      },
    );
  }
}
