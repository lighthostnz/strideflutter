# Technical Design Stride Super APP

## Table of Contents

- [1. Technology Overview](#1-technology-overview)
- [2. State Management](#2-state-management)
- [3. Architecture and Design Patterns](#3-architecture-and-design-patterns)
- [4. Application Architecture Decisions](#4-application-architecture-decisions)
- [5. Project Structure](#5-project-structure)
- [6. Navigation model](#6-navigation-model)
- [7. Conventions](#7-conventions)
- [8. Application Responsiveness](#8-application-responsiveness)
- [9. Internationalization](#9-internationalization)
- [10. Animation](#10-animation)
- [11. Frosted Glass Effect](#11-frosted-glass-effect)
- [12. Theme options](#12-theme-options)
- [13. Authorization](#13-authorization)
- [14. Authentication](#14-authentication)
- [15. gRPC Service](#15-grpc-service)
- [16. Exception Handling](#16-exception-handling)
- [17. Caching](#17-caching)
- [18. References](#18-references)

## Objective

The main objective of technical design is to understand what is the best way to build Flutter application. Technical design makes application more effective by forcing us to think through design and architecture patterns. The document will describe project structure, architecture, naming conventions and design pattern. Flutter gives the feasibility to create architecture as our own architecture but there might be a chance that the code may not be maintainable, testable or scalable. So we should use some architecture to make the app more maintainable, testable or scalable.

## Description

## 1. Technology Overview

### Chosen Technology

The Stride Application will be written in **Dart** language with the **Flutter** Framework. The version of Flutter that has been used in development is **Flutter 2.10.3 • channel stable** . The version of Dart that has been used in development is **Dart 2.16.0**

### Reason for Technology Decision

Flutter is Google's UI toolkit for building beautiful, natively compiled applications for mobile, web, desktop, and embedded devices from a single codebase. Flutter's architecture is based on the very popular reactive programming (it follows the same style as React). Flutter app will look and behave naturally on each state management technique, imitating their native components. It support hot reload and gives the native perfomance.

Flutter will enable **crossstate management technique** usage of the application on both Android and IOS. Flutter is Google's latest growing technology targeting to make cross state management technique more powerful. Hot reload feature is speeding up development process vs native implementations. Additionally, **Dart code is compiled into native** code thus making Flutter applications as fast as Native ones. The Community support is good and widespread. Flutter has the potential to go beyond Mobile.

[More info here](https://flutter.dev) and [here](https://www.thedroidsonroids.com/blog/flutter-vs-react-native-what-to-choose-in-2021#whatsflutterfor)

### Comparing Competing Technologies

#### Flutter vs React Native:

React Native combines the best parts of native development with React, a best-in-class JavaScript library for building user interfaces. It is used to develop applications for Android, Android TV, iOS, macOS, tvOS, Web, Windows and UWP by enabling developers to use React's framework along with native state management technique capabilities.

React primitives render to native state management technique UI, meaning your app uses the same native state management technique APIs other apps do. Learn once, write anywhere.

[More info here](https://reactnative.dev)

The papers below linked find **very close performances** and no significant difference between Flutter and React native.

[React Native vs Flutter](https://www.theseus.fi/bitstream/handle/10024/146232/thesis.pdf?sequence=1)

[Performance Comparison between React Native vs Flutter](https://www.diva-portal.org/smash/get/diva2:1349917/FULLTEXT01.pdf)

[Flutter vs other cross state management technique frameworks](https://betterprogramming.pub/react-native-vs-flutter-vs-ionic-46d3350f96ee)

But authors point out that **architecture wise, the Flutter engine’s C/C++ code is compiled with Android’s NDK and LLVM on iOS respectively, and any Dart code is AOT-compiled into native code during compilation**. Thus giving a better structure than React native as it uses bridge to native. Since React native is a more mature technology and Flutter is younger technology, we may see Flutter using more of its structural edge in the coming years.

### Conclusion

We are going with flutter because it's provide better performance than react native/cordova/ionic, more popularity than other cross/hybrid platform frameworks and good community support.

## 2. State Management

Managing state in an application is one of the most important and necessary process in the life cycle of an application. State is information that can be read synchronously. The state corresponds to the local data that a widget can hold which is available after the widget has rendered/rebuilded and also deciding which widget's state needs to be mutable (Stateful widget), and which needs to be immutable (Stateless Widget).

State management is combination of [reactive objects](https://www.ibm.com/docs/en/rhapsody/8.2?topic=model-reactive-objects) and [dependency injection](https://medium.com/flutter-community/dependency-injection-in-flutter-f19fb66a0740).

![Alt text](images/state-management-explainer.gif)

### setState

setState is default state management technique provided by flutter team. It notify the framework when internal state of the object has changed. Calling setState() is critical, because this tells the framework that the widget’s state has changed and that the widget should be redrawn whole `build` method. We should avoid repetitive and costly work in build() methods since build() can be invoked frequently when ancestor Widgets rebuild.

[More info here](https://flutter.dev/docs/development/ui/interactive)

### Provider

Provider is a state management helper. It’s a widget that makes some value – like a state model object – available to the widgets below it.
Provider is a wrapper around InheritedWidget to make them easier to use and more reusable. Provider is a lightweight but flexible dependency injection and state management tool. The latest version in market is 5.0.0. Provider providing simplified allocation/disposal of resources as well as lazy-loading. Provider contains states throught the app. Provider increased scalability for classes with a listening mechanism that grows exponentially in complexity. Provider introduce ChangeNotifierProxyProvider which is a ChangeNotifierProvider that builds synchronize a ChangeNotifier with external values.

[More info here](https://pub.dev/packages/provider)

#### Provider + ChangeNotifier

`Provider` is sugar syntax for `InheritedWidget` and the default reactive objects to work with this dependency injector are `ChangeNotifier` and `ValueListenable`.
Those object are part of the `Flutter foundation` package and require to much work for maintain unmutability for states.
Sometimes this can be a critical issue because if the origin of many race conditions in asynchronous processes.

#### Provider + StateNotifier

[StateNotifier](https://pub.dev/packages/state_notifier) is an alternative to `ChangeNotifier` that doesn't depent on Flutter.
In the beginning `StateNotifier` was faster then the second one, but after many contributions, `ChangeNotifer` is currently faster.
Despite that, [StateNotifier] offers a mechanism to maintain inmutability of the state, avoiding the posibility of race conditions as much as possible.

In addition, Both `Provider` and `StateNotifier` are from the same author, Remi Rousselet and also created [Flutter_state_notifier](https://pub.dev/packages/flutter_state_notifier)
for integrating these package for having a complete state management solution.

### BLoC

BLoC (Business Logic Component) is an architectural pattern based on separate components (BLoC components). The latest version in market is 7.0.0. BLoC components contain only business logic, which can easily be shared between different Dart apps. This architecture was introduced by Google at Google I/O 2019. BLoC makes it easy to separate the presentation layer from the business logic, making your code fast, easy to test, and reusable. Currently, the BLoC architecture is the most popular Flutter architecture. BLoC has immutable state because Immutability is used for performance reasons.

Bloc was designed with three core values in mind:

- Simple: Easy to understand & can be used by developers with varying skill levels.
- Powerful: Help make amazing, complex applications by composing them of smaller components.
- Testable: Easily test every aspect of an application so that we can iterate with confidence.

[More info here](https://bloclibrary.dev/#/)

### Riverpod

Riverpod is created by the same person who has created provider, `Remi Rousselet`.

If provider is a simplification of InheritedWidgets, then Riverpod is a reimplementation of InheritedWidgets from scratch.

It is very similar to provider in principle, but also has major differences as an attempt to fix the common problems that provider face. In riverpod, Reading objects is now compile-safe. No more runtime exception and also makes the pattern independent from Flutter.

Is it safe to use in production?

Yes, but with caution.

Riverpod recently left its experimental status, but it isn't fully stable either. The API may change slightly when more features are added, and some use-cases may not be as simple as they could be. But overall, you should be able to use Riverpod without trouble.

Riverpod is just dependency injection and we have to use `StateNotifier` for reactivity and state management solution.

[More info here](https://pub.dev/packages/riverpod)

#### Riverpod + StateNotifier

By default, Riverpod use `StateNotifer` as its default reactive object in counterpart to `Provider` (see the [provider section](#provider)).
This solution is completely independet of Flutter, but there are twe pagackes for integration::

- [Flutter_riverpod](https://pub.dev/packages/flutter_riverpod)
- [Hooks_riverpod](https://pub.dev/packages/hooks_riverpod)

#### Riverpod + ChangeNotifier

When we use `Flutter_riverpod`, this package include a `ChangeNotifierProvider` for working with `ChangeNotifier`.
This can help for some UI states.

### GetX

GetX is an extra-light and powerful solution for Flutter. It combines high-performance state management, intelligent dependency injection, and route management quickly and practically. GetX has mutable state which means we can change the state can be changed after we make the object. Mutability makes it harder to understand what your program is doing, and much harder to enforce contracts. This problem can occurs with provider as well.

[More info here](https://pub.dev/packages/get)

### Redux

Redux is designed to prevent bugs by making the State immutable. Redux may do well in synchronous situations but you can run into problems when you start doing things asynchronously and flutter is reactive framework and flutter is rely on events so it is asynchronously in other word.

[More info here](https://pub.dev/packages/flutter_redux)

### RiverBloc

This is combination of riverpod and bloc. Expose a BlocProvider based on Riverpod and can be use with flutter_riverpod or [hooks_riverpod](https://pub.dev/packages/hooks_riverpod) instead of [flutter_bloc](https://pub.dev/packages/flutter_bloc).

Riverbloc extends riverpod for having `BlocProvider` for replacing `StateNotifierProvider` allowing us to work riverpod + bloc with having advantages of bloc(Data persistance, memento, etc...).

[More info here](https://pub.dev/packages/riverbloc)

### Comparison

Comparison criteria:

- Persist immutable states - the state is mutable or immutable ?
- Support events - whether the particular state management technique is event based or not?
- Maintenance and Security Patches - How often the particular state management technique is patched and updated ?
- Future Prove - How much is the particular state management technique future proven, is it up-to date ?
- Popularity and Usage - Companies use which particular state management technique in a large scale projects ?
- Ease of use - How easy is to work with the particular state management technique ?
- Documentation and Community Support - How well is the particular state management technique documented and supported by community maintainers ?

The methodology that we have used for the comparison is widely used one:
​
| Description | Associated Score |
| ------------------------------------------------------------------------------------------------------- | ---------------- |
| Requirement exceeded | 10 |
| The standard software fully meets the requirement ie straight out of the box | 9 |
| Some modifications are required (e.g. configurations) | 8 |
| Meets requirements using a third party software (that is already integrated with the standard software) | 7 |
| Considerable modification required (e.g. interface development) | 6 |
| Meets requirements using a third party software (where an interface development is needed) | 5 |
| Future release (with a known release date or version number) | 4 |
| Source code will need to change (that the vendor will undertake) | 3 |
| Chargeable bespoke development (that you would have to pay for) | 2 |
| Future release (with no date – ie just on a wish list) | 1 |
| Requirement not met | 0 | ​ |

- The decision table based on the above Comparison criteria and scores is listed below:

This table is for reactive objects:

| Comparison Criteria                 | Weighting | setState | ChangeNotifier | BLoC | StateNotifier | getX | Redux |
| ----------------------------------- | --------- | -------- | -------------- | ---- | ------------- | ---- | ----- |
| Persist immutable states            | Essential | 0        | 0              | 10   | 0             | 0    | 9     |
| Support events                      | Essential | 0        | 0              | 10   | 0             | 0    | 6     |
| Maintenance and security patches    | Essential | 8        | 9              | 10   | 9             | 6    | 7     |
| Reliability (Code Coverage)         | Essential | 10       | 10             | 10   | 10            | 6    | 9     |
| Popularity and usage                | Desirable | 10       | 8              | 10   | 7             | 10   | 7     |
| Ease of use                         | Desirable | 10       | 9              | 9    | 8             | 9    | 8     |
| Documentation and Community Support | Desirable | 9        | 9              | 10   | 9             | 8    | 7     |
| Total                               |           | 47       | 45             | 69   | 43            | 39   | 53    |

As per above table BLoC looks like the most suitable for large scale apps. the state is immutable so we can overcome the leak of state. Bloc attempts to make state changes predictable by regulating when a state change can occur and enforcing a single way to change state throughout an entire application.

In counterpart, it is needed to look for the best dependency injection:

| Comparison Criteria                 | Weighting | InheritedWidget | Provider | Flutter_bloc | Riverpod | getX | Flutter_redux | Riverbloc |
| ----------------------------------- | --------- | --------------- | -------- | ------------ | -------- | ---- | ------------- | --------- |
| Extendable                          | Essential | 10              | 10       | 6            | 10       | 8    | 6             | 6         |
| Easy to extend                      | Essential | 6               | 10       | 10           | 10       | 7    | 10            | 10        |
| Maintenance and security patches    | Essential | 10              | 10       | 10           | 10       | 6    | 8             | 8         |
| Popularity and usage                | Desirable | 0               | 9        | 10           | 7        | 10   | 5             | 1         |
| Ease of use                         | Desirable | 6               | 8        | 9            | 8        | 10   | 8             | 10        |
| Documentation and Community Support | Desirable | 10              | 10       | 10           | 10       | 7    | 10            | 4         |
| Independent of Flutter BuildContext | Desirable | 0               | 0        | 0            | 10       | 7    | 0             | 10        |
| Total                               |           | 42              | 57       | 55           | 65       | 55   | 47            | 49        |

For dependency injection the best one here is `Riverpod`.

### Conclusion

`BLoC library` is a suit of packages for state management that provide tools for data persistance,
memento pattern (A.K.A. replay) and testing. Then, it will be easy to create a complex and large scale application with reactive objects.

**flutter_bloc** allows us to decompose our app's state into smaller, transform events in states.

It's simple, we can implement reactivity in our app without thinking much about the complexity behind subscriptions or lifecycles, instead, flutter_bloc abstract the complexity of streams and focus on the real, predictable interaction with our app. If we received events, an state need to be produced.

Another of its characteristics is that it's highly testable. The pattern used and its implemented components are completely testable and facilitate testing, avoiding the use of ui components that need context and must not be evaluated during unit testing. Also we can use **bloc_test**, it's a utility library that removes all the complexity oof testing reactive code, and allows us to unit test our code with almost no setup required.

It has a lot of documentation, tutorials and implementation examples.

It's very popular and used by thousands, flutter_bloc has helped companies of all sizes to scale and release their products successfully.

**flutter_bloc** has even been named a Flutter Favorite by the Flutter Ecosystem Committee, who consider metrics like overall package score, feature completeness, usability, and more.

In conclusion, the project will be based on `Flutter bloc` because it is currently the best option for scalable projects.

## 3. Architecture and Design Patterns

Software architecture refers to the fundamental structures of our Stride app and the discipline of creating such structures and systems. Each structure comprises layers, relations among them, and properties of both layers and relations. It functions as a blueprint for the system and the developing project, laying out the tasks necessary to be executed by the design teams.

Software architecture description involves the principles and practices of modeling and representing architectures, using mechanisms such as architecture description languages, architecture viewpoints, and architecture frameworks.

Documenting software architecture facilitates communication between team members, captures early decisions about the high-level design, and allows reuse of design components between projects.

### 3.1 Application Architecture

- Clean Architecture
- Domain Driven Architecture
- Clean Architecture + Domain Driven Design
- Test Driven Architecture + Domain Driven Architecture

#### 3.1.1 Clean Architecture

Clean architecture is architecture based on the book and blog by Uncle Bob. It is a combination of concepts taken from the Onion Architecture and other architectures. The main focus of the architecture is separation of concerns and scalability. It consists of four main modules: App, Domain, Data, and Device.

![Alt text](images/CleanArchitecture.jpg)

Here you can see there are some layers and data goes from outer layer to inner layer. Entity is data model class which contains all the necessary data which comes from backend API.
Use cases contains application business logic like whether we should goes to local cache or server to fetch the data. controller and presenter layer is state management layer which is bloc in our case which emits the event and yield the state from UI.

Outer layer is responsible for communication between outside of the app and our app like camera, sensor etc... Whatever data comes from this layer directly map to entity.

On the layered "onion" image below, the **horizontal arrows --->** represent dependency flow. For example, **Entities** do not depend on anything, **Use Cases** depend only on **Entities** etc.

Clean architecture is testable. The business rules can be tested without the UI, Database, Web Server, or any other external element.

Independent of UI. The UI can change easily, without changing the rest of the system. A mobile UI could be replaced with a console UI, for example, without changing the business rules.

#### Dependency rules

There are many dependency injection libraries out there like `Injector`, `Kiwi`, `GetIt` and `Riverpod`.

GetIt is: (we will get the future proof, maintainable, and up-to date library)

- Extremely fast (O(1))
- Easy to learn/use
- Doesn't clutter your UI tree with special Widgets to access your data like provider or Redux does.

[More info here](https://pub.dev/packages/get_it)

On the other hand, `Riverpod` satisfies the same requirementes than `GetIt` and also has:

- `FamilyProvider`s for defining providers dynamically in runtime
- `AutodisposeProvider`s for automatically removing the injected object when they are not used anymore
- `AutoDisposeFamilyProvider`s that give us the both previous functionallities at the same time

#### There are four layers in this architecture:

1. Domain layer:

   The Domain module defines the business logic of the application. It is a module that is independent from the development platform i.e. it is written purely in the programming language and does not contain any elements from the platform. In the case of Flutter, Domain would be written purely in Dart without any Flutter elements. The reason for that is that Domain should only be concerned with the business logic of the application, not with the implementation details. This also allows for easy migration between platforms, should any issues arise.

2. Presentation layer:

   Presentation is the layer outside Domain. Presentation crosses the boundaries of the layers to communicate with Domain. However, the Dependency Rule is never violated. Using polymorphism, App communicates with Domain using inherited class: classes that implement or extend the Repositories present in the Domain layer. Since polymorphism is used, the Repositories passed to Domain still adhere to the Dependency Rule since as far as Domain is concerned, they are abstract. The implementation is hidden behind the polymorphism.

   We are emitting event from this layer to bloc and bloc goes to `domain` layer for furture operations.

3. Data layer:

   Represents the data-layer of the application. The Data module, which is a part of the outermost layer, is responsible for data retrieval. This can be in the form of API calls to a server, a local database, or even both.

4. Device layer:

   Part of the outermost layer, Device communicates directly with the platform i.e. Android and iOS. Device is responsible for Native functionality such as GPS and other functionality present within the platform itself like the filesystem. Device calls all Native APIs.

#### Pros:

- Consistent business logic through the app
- Swappable data persistence, external systems integration, and presentation
- Enables carving out a vertical feature slice into a separate service (microservice, nanoservice/serverless) without much difficulty
- Promotes more testable design patterns (Core business logic tends to always be testable)

#### Cons:

- Requires more intentional design (you can't reference data persistence APIs directly in your business logic)
- Due to the business logic being agnostic of the outer layers you can loose optimizations in being closer to library features and implementations with the benefit of looser coupling
- Can be overkill when only a CRUD app is needed

[More info here](https://pub.dev/packages/flutter_clean_architecture)

#### 3.1.2 Domain Driven Architecture

Domain-Driven Design (DDD) has a good separation into layers which brings beautiful traits of ease of navigation and testability. So no, we still don't put all of our code into the UI or state management. We are using BLoC as a state management technique. Domain-Driven Design (DDD) is based on BLoC pattern architecture. It is a more advanced version of the BLoC pattern. Let’s say if we have multiple sub applications within an app then we can create a folder inside lib and implement this architecture for that particular sub application.

![Alt text](images/ddd_arch.png)

There are a few things that I couldn't fit on the diagram. Namely:

- Arrows represent the flow of data. This can be either uni-directional or bi-directional.
- The domain layer is completely independent of all the other layers. Just pure business logic & data.

We are thinking about domain which is also known as feature and try to keep everything inside that feature which is associated with it like presentation, logic, API call etc...

#### There are four layers in this architecture:

1. Presentation:

   The presentation contains all widgets. it’s responsibility is to figure out how to render itself based on one or more bloc states. In addition, it should handle user input and application lifecycle events.

2. Provider:

   Provider layer contains application logic like Business Logic Layer. The Provider layer's responsibility is to respond to input from the presentation layer with new states. This layer can depend on one or more repositories to retrieve data needed to build up the application state. You aren't going to find any UI code, network code, or database code here.

3. Domain:

   Domain layer contains Business logic like everything which is not Flutter/server/device dependent goes into the domain layer. It’s all about data models and failure models when some exceptions occur.

4. Infrastructure:

   Infrastructure deals with APIs, Firebase libraries, storage (Shared preference or local_storage, internal storage), databases and device sensors. Infrastructure layer is like the Data layer. This layer is the lowest level of the application and interacts with databases, network requests, and other asynchronous data sources.

#### Pros:

- Patterns
- Business Logic

#### Cons:

- Higher Learning Curve
- Instant time consumption
- When there are so many domain interconnections then it might make dreadlocks.
- Not ideal if complexity isn’t an issue.

[More info here](https://resocoder.com/2020/03/09/flutter-firebase-ddd-course-1-domain-driven-design-principles/)

#### 3.1.3 Clean Architecture + Domain Driven Design

We are using the same principles as mentioned in clean architecture section. We will follow the dependency injection rules as well.

Here we are using same layers as mentioned in clean architecture and think about domain first e.g. `Authentication`, `community` etc...

**The only difference is we have `Domain`, `Presentation`, `Data`, and `Device` layers also here with the same principle of clean architecture. We define this layers for each module/feature.** So in this way it's combination of clean architecture and domain driven design.

This architecture relies heavily on some widely-known principles, the [SOLID](https://en.wikipedia.org/wiki/SOLID) Principles, which I recommend you to have a look at. DDD says nothing about the architecture of your application. Databases, delivery such as HTTP or gRPC and controllers are not important. Here is where our two concepts combine forces for a brighter future. Using DDD to model the entities and use cases mentioned in clean architecture diagram has worked out well for us.

![Alt text](images/agile-architecture-ddd-and-cqrs-62-638.jpg)

Conforming to these simple rules is not hard, and will save you a lot of headaches going forward. By separating the software into layers, and conforming to The Dependency Rule, you will create a system that is intrinsically testable, with all the benefits that implies. When any of the external parts of the system become obsolete, like the database, or the web framework, you can replace those obsolete elements with a minimum of fuss.

let's understand this picture clearly:

![Alt text](images/image_of_clean_with_domain.png)

Here, there are three layers mostly `presentation`, `domain` and `data`. Data should comes from data layer to presentation layer and call flows should goes presentation layer to data later.

Presentation layer should contains only UI stuff like widgets and screens. Domain layer should contains entity (which is data model/POJO class and it's define how we want to data for presentation layer), mapper (sometimes we need data which might combine two POJO class), BLoC (state management layer), Abstract repository, and viewmodel. Data layer should contains the implementation of repository (which decides that whether we need raw data from local or remote).

**NOTE: It should not communicate direct from presentation layer to data layer and data should not go from data layer to presentation layer directly.**

[More info here](https://mastanca.medium.com/clean-architecture-ddd-a-mixed-approach-773ab4623e14) and [here](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)

#### 3.1.4 Test Driven Architecture + Domain Driven Architecture

Test driven architecture converts the software requirement to test cases and try to pass tests while developing apps. If one test is passed then we can conclude that this test related functionality is done. We split the whole app as a feature and the layers which we will discuss just a bit is for one feature.

These layers depend on features. Some features don't need a Data layer as they are not working outside of the app. So in this case, we just need two layers. These layers should depend on abstraction instead of concrete implementation, especially the repository which will be present in the domain layer as an abstract class and data layer as a concrete class. Let’s say if we have multiple sub applications within an app then we can create a folder inside lib and implement this architecture for that particular sub application.

Test driven architecture is the process of creating software by writing tests in advance to prove the implementation fulfills the requirements. For most developers, it’s not something that comes naturally and the hardest part is to get used to it. But the good news is that the process of TDD is rhythmic, and because of that, I will try to show you by example how easy it is when you feel the rhythm. Basically, through all your work you need to repeat 3 steps/colors:

1. Write a failing test (Red phase)
2. Make the test pass — Write just enough code to pass the test (Green phase)
3. Improve the code — Clean up the mess (Refactoring phase)

<img src="images/TDD.png" alt="drawing" width="300"/>

#### This is brief overview of architecture

![Alt text](images/Clean-Architecture-Flutter-Diagram.png-2.jpg)

#### There are three layers in this architecture:

1. Presentation:

   The presentation contains all widgets. it’s responsibility is to figure out how to render itself based on one or more bloc states. In addition, it should handle user input and application lifecycle events. Presentation layer contains widgets, BLoC and it’s event and state.

2. Domain:

   Domain layer contains Business logic like everything which is not Flutter/server/device dependent goes into the domain layer. It’s all about data models and failure models when some exceptions occur. There will not be much business logic. This layer contains abstract repository and data models. Whatever event occurs in the bloc it will ask a repository for business logic.

3. Data:

   Data layer deals with APIs, Firebase libraries, storage (Shared preference or local_storage, internal storage), databases and device sensors. The Data layer is like the Data layer.
   Data layer contains the implementation of the repository which we have defined in the domain layer. This layer also contains the operation with disk, memory, shared preference or API. This layer is the lowest level of the application and interacts with databases, network requests, and other asynchronous data sources.

#### Pros:

- You only write code that’s needed
- More modular design
- Easier to maintain
- Easier to refactor
- High test coverage
- Less debugging

#### Cons:

- slow process
- Tests got to be maintained when requirements change

[More info here](https://resocoder.com/2019/08/27/flutter-tdd-clean-architecture-course-1-explanation-project-structure/)

### 3.1.5 conclusion

As we discuss all possible architecture of flutter, we can see clean architecture is best suite for us because of it's goal is to `Separation of concerns`, `Scalability` and `Easy to understand`.

### 3.2 Design Pattern

- BLoC Pattern
- MVVM
- MVC

#### 3.2.1 BLoC Pattern

Bloc makes it easy to separate presentation from business logic, making your code fast, easy to test, and reusable. It is a state management system for Flutter. Bloc tries to make state changes predictable by emitting the event from UI when a state change can occur and enforcing a single way to change state throughout an entire application.

![Alt text](images/bloc_architecture.png)

<img src="images/bloc_pattern.png" alt="drawing" width="400"/>

When building production quality applications, managing state becomes critical.

As developers we want to:

- know what state our application is in at any point in time.
- easily test every case to make sure our app is responding appropriately.
- record every single user interaction in our application so that we can make data-driven decisions.
- work as efficiently as possible and reuse components both within our application and across other applications.
- have many developers seamlessly working within a single code base following the same patterns and conventions.
- develop fast and reactive apps.

Bloc was designed to meet all of these needs and many more.

Bloc was designed with three core values in mind:

1. Simple: Easy to understand & can be used by developers with varying skill levels.
2. Powerful: Help make amazing, complex applications by composing them of smaller components.
3. Testable: Easily test every aspect of an application so that we can iterate with confidence.

#### There are three layers in this architecture:

1. Presentation:

   The presentation contains all widgets. it’s responsibility is to figure out how to render itself based on one or more bloc states. In addition, it should handle user input and application lifecycle events.

2. Business Logic:

   The Business Logic layer's responsibility is to respond to input from the presentation layer with new states. This layer can depend on one or more repositories to retrieve data needed to build up the application state. You aren't going to find any UI code, network code, or database code here.

   Think of the business logic layer as the bridge between the user interface (presentation layer) and the data layer. The business logic layer is notified of events/actions from the presentation layer and then communicates with repository in order to build a new state for the presentation layer to consume.

3. Data Layer:

   The data layer's responsibility is to retrieve/manipulate data from one or more sources like APIs, Firebase libraries, storage (Shared preference or local_storage, internal storage), databases and device sensors. This layer is the lowest level of the application and interacts with databases, network requests, and other asynchronous data sources.

   The data layer can be split into two parts:

   1. Repository
   2. Data Provider

You first need to organize a Flutter app according to an architectural pattern like DDD, MVVM, or Clean. The BLoC architecture then works more like a pattern that further organizes the data flows in your app as shown below.

#### Pros:

- Easy to separate UI from logic
- Easy to test code
- Easy to reuse code
- Good performance

#### Cons:

- More boilerplate code, But it is worth it for larger apps
- it's not architecture itself

[More info here](https://bloclibrary.dev/#/architecture)

#### 3.1.2 MVVM

![](images/mvvm.png)

- Model–view–viewmodel is a software architectural pattern that facilitates the separation of the development of the graphical user interface – be it via a markup language or GUI code – from the development of the business logic or back-end logic so that the view is not dependent on any specific model platform.

- The **Model** represents the application data and business logic. One of the recommended implementation strategies of this layer is to expose its data through observables that are completely decoupled from ViewModel or any other observer/consumer.
- The **ViewModel** interacts with model and also prepares observable(s) that can be observe by a View. The ViewModel can optionally provide hooks to the view to pass events to the model.
  One of the important implementation strategies of this layer is to decouple it from the View. i.e, The viewModel should not be aware about the view who is interacting with.
- The **View** observe (or subscribe to) the ViewModel to get data in order to update the UI elements accordingly.

### Pros

- MVVM facilitates easier parallel development of a UI and the building blocks that power it.

- MVVM abstracts the View and thus reduces the quantity of business logic (or glue) required in the code behind it.

- The ViewModel can be easier to unit test than in the case of event-driven code.

- The ViewModel (being more Model than View) can be tested without concerns of UI automation and interaction.

### Cons

- For simpler UIs, MVVM can be overkill.

- While data bindings can be declarative and nice to work with, they can be harder to debug than imperative code where we simply set breakpoints.

- Data bindings in nontrivial applications can create a lot of bookkeeping. We also don’t want to end up in a situation where bindings are heavier than the objects being bound to.

- In larger applications, it can be more difficult to design the ViewModel up front to get the necessary amount of generalization.

#### 3.1.3 MVC

![](images/mvc.png)

- The **Model** is lowest level of the pattern which is responsible for maintaining data.
- The **View** is responsible for displaying all or a portion of the data to the user.
- The **Controller** is responsible to controlling the interactions between the Model and the View.

In our application with the **BLoC** and **Provider** we will use **MVC** structure. The main reason behind choosing **MVC** is BLoC already have the **ViewModel** layer in its structure. So additionally we have no need to create ViewModel and make it more complex.

The MVC is a very clean architecture and is easy to maintain. The above project structure is designed keeping in mind the MVC.

### Pros

- The Application development gets faster.
- The MVC is easy to debug because we have many levels written properly in the application.
- The MVC is easy to manage in our code according to complex navigation structure.

### Cons

- Again the MVC will increase the boilerplate code.
- We must have strict rules on methods otherwise memory leak and state leak will occur.

## 5. Application Architecture Decisions

Decision table helps choosing the final item out of many based on various factors.

Here we have tried to compare seven architectures and evaluated based on pros and cons. The weightage is given out of 10 to each one.

Comparison criteria:

- Separation of concerns - How good the perticular architecture Separate UI to bussiness logic ?
- Scalability - How good the perticular architecture scales ?
- Maintenance and Security Patches - How often the perticular architecture is patched and updated ?
- Future Prove - How much is the perticular architecture future proven, is it up-to date ?
- Popularity and Usage - Companies use which perticular architecture in a large scale projects ?
- Ease of use - How easy is to work with the perticular architecture ?
- Documentation and Community Support - How well is the perticular architecture documented and supported by community maintainers ?

The methodology that we have used for the comparison is widely used one:
​
| Description | Associated Score |
| ------------ | ------------------------------------------------------------------------------------------------------ |
| Requirement exceeded | 10 |
| The standard software fully meets the requirement ie straight out of the box | 9 |
| Some modifications are required (e.g. configurations) | 8 |
| Meets requirements using a third party software (that is already integrated with the standard software) | 7 |
| Considerable modification required (e.g. interface development) | 6 |
| Meets requirements using a third party software (where an interface development is needed) | 5 |
| Future release (with a known release date or version number) | 4 |
| Source code will need to change (that the vendor will undertake) | 3 |
| Chargeable bespoke development (that you would have to pay for) | 2 |
| Future release (with no date – ie just on a wish list) | 1 |
| Requirement not met | 0 |​

The decision table based on the above Comparison criteria and scores is listed below:

| Comparison Criteria                 | Weighting | Clean Architecture | Domain Driven Architecture | Clean Architecture + Domain Driven Design | Test Driven Architecture + Domain Driven Architecture |
| ----------------------------------- | --------- | ------------------ | -------------------------- | ----------------------------------------- | ----------------------------------------------------- |
| Separation of concerns              | Essential | 9                  | 9                          | 10                                        | 6                                                     |
| Scalability                         | Essential | 8                  | 8                          | 10                                        | 8                                                     |
| Maintenance and Security Patches    | Essential | 9                  | 9                          | 9                                         | 9                                                     |
| Future Prove                        | Essential | 9                  | 9                          | 9                                         | 9                                                     |
| Popularity and Usage                | Desirable | 9                  | 9                          | 9                                         | 9                                                     |
| Ease of use                         | Desirable | 8                  | 8                          | 9                                         | 8                                                     |
| Documentation and Community Support | Desirable | 9                  | 9                          | 9                                         | 9                                                     |
| Total                               |           | 61                 | 61                         | 65                                        | 58                                                    |

Based on the decision table above and after careful consideration of all above architecture we as a team think that the 'clean Architecture + Domain driven architecture' will be best suited for our use case because it's scalable by default. it's mainly targeting of `Separation of concerns`.

Since we are going to support different modules such as authentication, wallet and micro apps etc we think that using the 'Bloc architecture' with feature/domain driven directory structure is best option.

The feature/domain driven structure enables us to scale the project by having self-contained features. In the first release we will only have a single micro app but in future the app will become more complex as we will have number of other third party micro apps.

### Other architecture

Most of the different approaches are based on the packages in the [pub.dev](https://pub.dev/), but the analysis says that they are not a completed by themself.
The best solution becomes mixin different approaches: `Bloc` and `Riverpod`

In addition, companies like [Very Good Venture (VGV)](https://verygood.ventures/) has the `philosophy` of divide all in package.
One exampple is the [Photobooth app](https://github.com/flutter/photobooth).

The application is divided by features, but they make the application as light as possible defining many reusable packages/plugins that could be in another repository if necessary for another project o white label app.

Also, every app may have a customized domain, data and UI, then it can be
developed in the same Git repository of the app.

## 5. Project Structure

Creating a good project structure is the first and most important part of organising your project. If you are unable to separate your code for different segments then you may get lost at one point. Therefore you need to maintain your code hierarchy properly.
Common issues with improper structure are given below.

- Unable to find a specific file
- Write code repeatedly
- Mixing up User Interface and back-end code
- Unlimited local variable

There is no rule to organise a flutter project. But when creating a structure we referenced the existing architecture as well as clean architecture to avoid the above issues.

### Here is the project structure for the proposed architecture.

Every feature/microapp is very well separated from each other and inside the feature, the UI code and business logic is separated as per 'clean Architecture + Domain driven architecture' thereby providing the separation of concern.

        ├── lib
        │   ├── app
        │   │   ├── core
        │   │   │    ├── widgets
        │   │   |    |     └── buttons.dart
        │   │   ├── feature
        |   │   │       ├── authentication
        |   │   │       |        ├── data
        |   │   │       |        |     ├── model
        |   │   │       |        |     |     └── user_model.dart
        |   │   │       |        |     ├── repository
        |   │   │       |        |     |     └── authentication_repository_impl.dart
        |   │   │       |        |     ├── datasource
        |   │   │       |        |     |     └── authentication_remote_data_source.dart
        |   │   │       |        ├── domain
        |   │   │       |        |     ├── entities
        |   │   │       |        |     |     └── user.dart
        |   │   │       |        |     ├── repository
        |   │   │       |        |     |     └── authentication_repository.dart
        |   │   │       |        ├── presentation
        |   │   │       |        |     ├── bloc
        |   │   │       |        |     |     └── authentication_bloc.dart
        |   │   │       |        |     |     └── authentication_event.dart
        |   │   │       |        |     |     └── authentication_state.dart
        |   │   │       |        |     ├── view
        |   │   │       |        |     |     └── login_screen.dart
        |   │   │       |        |     ├── widgets
        |   │   │       |        |     |     └── user_name_text_field.dart
        │   │   ├── router
        │   │   │    └── router.dart
        │   │   │    └── router.gr.dart
        │   │   ├── localization
        │   ├── config
        │   │   ├── app.dart
        │   │   ├── app.dart
        │   ├── di
        │   │   ├── setup_locator
        │   ├── bloc_observer.dart
        │   └── main.dart
        ├── pubspec.lock
        ├── pubspec.yaml

## 6. Navigation model

Navigation is a key part of the user experience for any mobile application. Due to the limited screen real estate on mobile devices, users will constantly be navigating between different screens.

The iOS navigation experience is often built-around a UINavigationController, which uses a stack-based approach to shifting between screens. On Android, the Activity stack is the central means to shift a user between different screens, although the use of Jetpack Navigation with a single-Activity architecture is a new and recommended approach. Unique transitions between screens in these stacks can help give your app a unique feel.

Just like the native SDKs, cross-state management technique development frameworks such as Flutter must provide a means for your app to switch between screens. In most cases, you’ll want the navigation approach to be consistent with what the users of each state management technique have come to expect – to have a native experience.

### Navigate with named routes

if you need to navigate to the same screen in many parts of your app, this approach can result in code duplication. The solution is to define a named route, and use the named route for navigation.

To work with named routes, use the Navigator.pushNamed() function. This example replicates the functionality from the original recipe, demonstrating how to use named routes using the following steps:

- Create two screens.
- Define the routes.
- Navigate to the second screen using Navigator.pushNamed().
- Return to the first screen using Navigator.pop().

### Pass arguments to a named route

The Navigator provides the ability to navigate to a named route from any part of an app using a common identifier. In some cases, you might also need to pass arguments to a named route. For example, you might wish to navigate to the /user route and pass information about the user to that route.

You can accomplish this task using the arguments parameter of the Navigator.pushNamed() method. Extract the arguments using the ModalRoute.of() method or inside an onGenerateRoute() function provided to the MaterialApp or CupertinoApp constructor.

This recipe demonstrates how to pass arguments to a named route and read the arguments using ModalRoute.of() and onGenerateRoute() using the following steps:

- Define the arguments you need to pass.
- Create a widget that extracts the arguments.
- Register the widget in the routes table.
- Navigate to the widget.

## 7. Conventions

While working on a project, we all should speak the same words to refer something.
Here we are declaring some common words that we will use.

- Super app: The app which will host the micro apps
- Micro app: The app developed by third party and will be integrated into the super app.

### Naming conventions

- `UpperCamelCase` names capitalize the first letter of each word, including the first.

- `lowerCamelCase` names capitalize the first letter of each word, except the first which is always lowercase, even if it’s an acronym.

- `lowercase_with_underscores` names use only lowercase letters, even for acronyms, and separate words with `_`.

- file name also should be lowercase_with_underscores.

- PREFER using `_`, `__`, etc. for unused callback parameters.

[More info here](https://dart.dev/guides/language/effective-dart/style)

## 8. Application Responsiveness

we need to rethink how it will look when the UI is rendered on a widescreen. The
app should be able to respect varying screen sizes and provide different UI/UX
for a rich experience.

```dart
class ResponsiveWidget extends StatelessWidget {
   final Widget smallScreen;
   final Widget mediumScreen;
   final Widget largeScreen;

   const ResponsiveWidget({Key key,
   this.mediumScreen,
   @required this.smallScreen,
   this.largeScreen})
      : super(key: key);

   @override
   Widget build(BuildContext context) {
   //Returns the widget which is more appropriate for the screen size
      return LayoutBuilder(builder: (context, constraints) {
         if (constraints.maxWidth > 1200) {
            return largeScreen;
         } else if (constraints.maxWidth > 800 && constraints.maxWidth < 1200) {
            //if medium screen not available, then return large screen
            return mediumScreen ?? largeScreen;
         } else {
            //if small screen implementation not available, then return large screen
            return smallScreen;
         }
      });
   }
}
```

The _LayoutBuilder_ widget’s builder function is called whenever the parent
widget passes different layout constraints. That means whenever the screen
resolution changes, it provides the `constraints`which determines the width and
provides various UI accordingly.

```dart
class MyApp extends StatelessWidget {

   @override
   Widget build(BuildContext context) {

      return App(
         client: client,
         child: MaterialApp(
            ...
            home: ResponsiveWidget(
            smallScreen: HomeSmallScreen(),
            largeScreen: HomeLargeScreen(),
            ),
         ),
      );
   }
}
```

Here we wrapped the initial UI inside the _ResponsiveWidget()_ and
_HomeLargeScreen()_ is given as a widget to display on a large screen.

[More info here](https://blog.codemagic.io/building-responsive-applications-with-flutter/)

## 9. Internationalization

- By default, Flutter only provides **US English localisations**. To add support for other languages, an application must specify additional MaterialApp (or CupertinoApp) properties, and include a package called **flutter_localizations**.
- As of November 2020, this package supports 78 languages.

#### Package

```dart
dependencies:
  flutter:
    sdk: flutter
  flutter_localizations: # Add this line
    sdk: flutter
```

#### Method

After adding the flutter_localizations package, we need to add couple of line in MaterialApp widget in main.dart

```dart
localizationsDelegates: [
   GlobalMaterialLocalizations.delegate,
   GlobalWidgetsLocalizations.delegate,
   GlobalCupertinoLocalizations.delegate,
 ],
 supportedLocales: [
    const Locale('en', ''), // English, no country code
    const Locale('tr', ''), // Turkey, no country code
    const Locale.fromSubtags(languageCode: 'zh'), // Chinese *See Advanced Locales below*
    // ... other locales the app supports
  ],
```

- After adding the code above, the Material and Cupertino packages should now be correctly localized in one of the 78 supported locales. Widgets should be adapted to the localized messages, along with correct left-to-right and right-to-left layout. Try switching the target state management technique’s locale to Arabic (ar) and notice that the messages should be localized and widgets are laid out with right-to-left layout in mind.
- The elements of the **localizationsDelegates** list are factories that produce collections of localized values. **GlobalMaterialLocalizations.delegate** provides localized strings and other values for the Material Components library. **GlobalWidgetsLocalizations.delegate** defines the default text direction, either left-to-right or right-to-left, for the widgets library.
- We can add our own localized message also for that we need to use below package and enable the **generate** flag

```dart
intl: ^0.17.0
flutter:
  generate: true
```

- Now we need to add new yaml file to the root directory of the Flutter project called l10n.yaml with the following content:

```
arb-dir: lib/l10n
template-arb-file: app_en.arb
output-localization-file: app_localizations.dart
```

- This file configures the localization tool. Now we have to add `app_en.arb` template file. For example:

```json
{
  "helloWorld": "Hello World!",
  "@helloWorld": {
    "description": "The conventional newborn programmer greeting"
  }
}
```

and also add template file for the language which you want to support in application like

```json
{
  "language": "Māori",
  "helloWorld": "kia ora te ao",
  "logoName": "Stride",
  "app_settings": "tautuhinga taupānga",
  "ok": "pai",
  "cancel": "whakakore",
  "dark_mode": "aratau pouri",
  "themes": "kaupapa"
}
```

Now run `flutter pub get` from terminal and you can see generated files in ${FLUTTER_PROJECT}/.dart_tool/flutter_gen/gen_l10n.

Now, Add the import statement on app_localizations.dart and AppLocalizations.delegate in your call to the constructor for MaterialApp

```dart
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

return MaterialApp(
  title: 'Localizations Sample App',
  localizationsDelegates: [
    AppLocalizations.delegate, // Add this line
    GlobalMaterialLocalizations.delegate,
    GlobalWidgetsLocalizations.delegate,
    GlobalCupertinoLocalizations.delegate,
  ],
  supportedLocales: [
    const Locale('en', ''), // English, no country code
    const Locale('es', ''), // Spanish, no country code
  ],
  theme: ThemeData(
    primarySwatch: Colors.blue,
  ),
  home: MyHomePage(),
);
```

In UI, we have to use `AppLocalizations` class for getting the current language text.

```dart
Text(AppLocalizations.of(context)!.helloWorld);
```

- So this is how we can as multi language support in our app.

- For depth understanding and Integration process please check this link. [Internationalizing Flutter apps](https://flutter.dev/docs/development/accessibility-and-localization/internationalization#:~:text=By%20default%2C%20Flutter%20only%20provides,this%20package%20supports%2078%20languages.)

- We need to add all localize file name in above yaml file.

**NOTE: Internationalization is not part of MVP, but for future purpose.**

## 10. Animation

The animation is a very powerful and important concept in Flutter. We cannot imagine any mobile app without animations. When you tap on a button or move from one page to another page are all animations. Animations enhance user experiences and make the applications more interactive.

Flutter provides excellent support for animation and there are two main categories of animation in mobile apps:

- **Code-based**
- **Drawing-based animations**

**Code-based**

Code-based animations tend to focus on animating the existing widget, such as container, row, column, stack, etc. It allows you to change the size, scale, position, etc. of the widget.

For example, you can move a picture of a product from the shopping list to the cart icon with code-based animations. In Flutter, you can create code-based animations using either implicit or explicit animations.

### Implicit animations

Implicit animation falls under code-based animations. It’s usually very simple to implement compared to other types of animation. All you need to do is set a new value and hit a setState to animate the widget to the new value.

### Explicit animations

Explicit animations also fall under the category of code-based animations. It’s called explicit because you have to explicitly start it.

**Drawing-based animations**

Drawing-based animations are, quite simply, used to animate drawings. It is usually done using a custom painter or any other animation framework, such as Rive.

### Prebuilt animation

1. Hero Animation

The hero refers to the widget that flies between screens. We can use Hero widget to archieve hero animation. We need to provide the same tag to those hero widget in which we want animation.

It's kinda fly the hero from one screen to another as you can see in this GIF.

```dart
Hero(
  tag: photo, // This shu
  child: Material(
    color: Colors.transparent,
    child: InkWell(
      onTap: onTap,
      child: Image.asset(
        'photo',
        fit: BoxFit.contain,
      ),
    ),
  ),
),
```

<img src="images/radial_hero_animation.gif" alt="drawing" width="300"/>

2. FadeInImage

When displaying images using the default Image widget, you might notice they simply pop onto the screen as they’re loaded. This might feel visually jarring to your users.

FadeInImage is something like while loading the image from network it's showing the placeholder and once image is fetched successfully then it show with some rendering animation.

FadeInImage works with images of any type: in-memory, local assets, or images from the internet.

```dart
FadeInImage.assetNetwork(
  placeholder: 'assets/loading.gif',
  image: 'https://picsum.photos/250?image=9',
),
```

<img src="images/fading-in-asset-demo.gif" alt="drawing" width="300"/>

[More info here](https://flutter.dev/docs/development/ui/animations)

## 11. Frosted Glass Effect

- The frosted glass implementation for **Stride app** is that the total app is acting as a frosted glass panel showing the operating system wallpaper as the background.
- To achieve this, first we need to make the entire application transparent.
- Flutter does not support transparent background so we have to go to the native Android binding and make the android background of the flutter screen transparent.
- We have created an image(frosted_background.png) that is like frosted glass and is set on the background of our screen.
- We can use the [GlassMorphism Pub Dev](https://pub.dev/packages/glassmorphism/) library to create frosted glass containers and list tiles.
- The overall conclusion is that we will use the existing implementation only.

## 12. Theme options

### Theme customization by users

If user select any color option from UI then we can emit one event to `ThemeBloc` and though bloc we can store the color's name to database and use that color's name to `MaterialApp` widget to reflect to whole app.

We are giving theme options to user for selecting several colors theme.

### There are two ways to turn on the dark mode/light mode in any app

1. Adding a custom option to change to the dark mode (Eg: Twitter, Medium app)

```dart
MaterialApp(
   theme: ThemeData.light(),
   darkTheme: ThemeData.dark(),
   home: MyHomePage(),
);
```

Here we have `theme` and `darktheme` parameter inside `MaterialApp` where we can provide the dark ThemeData in `darkTheme` and light `ThemeData` in theme if we want our app to change the theme according to the system preferences.

2. Depends on the Phone system setting (Eg: Instagram)

We have a custom button or something to change the dark theme then we just have to put some condition to change it and also while defining theme, you can select the accentColor and primaryColour based on your need. The important part is we have to mentioned the brightness whether is dark or light for each ThemeData class.

```dart
ThemeData _darkTheme = ThemeData(
   accentColor: Colors.red,
   brightness: Brightness.dark,
   primaryColor: Colors.amber,

);

ThemeData _lightTheme = ThemeData(
   accentColor: Colors.pink,
   brightness: Brightness.light,
   primaryColor: Colors.blue
);
```

We have to use any local storage mechanism like shared preference, hive or moor.

[More info here](https://itnext.io/an-easy-way-to-switch-between-dark-and-light-theme-in-flutter-fb971155eefe)

## 13. Authentication

To build a communication bridge between your Flutter app and Auth0, you need to set up a callback URL to receive the authentication result in your application after a user logs in with Auth0. A callback URL is a mechanism by which an authorization server communicates back to your application.

### Authentication via OAuth 2.0

OAuth 2.0 is the industry-standard protocol for authorization. It focus on client developer simplicity while provider special auth flow for apps. The OAuth 2.0 authorization framework enables a third-party application to obtain limited access to an HTTP service, either on behalf of a resource owner by orchestrating an approval interaction between the resource owner and the HTTP service, or by allowing the third-party application to obtain access on its own behalf.

We need to call the API endpoint for authentication using http/dio plugin.apps can register a custom URL scheme such `www.example.com/auth` and we might need to pass the token for security purpose.

We have to use third party plugin to call our custom URL scheme like http or dio

Install the http plugin:

```dart
dependencies:
  http: <latest version>
```

Here you can see one code snippet of how it's works

```dart
Future<ApiResponse> getUserDetails(String userId) async {
  ApiResponse _apiResponse = new ApiResponse();
  try {
      final http.Response response = await http.post(
         'www.example.com/auth',
         headers: <String, String>{
         'Content-Type': 'application/json; charset=UTF-8',
         },
         body: jsonEncode({'username': $username, 'password': $password}),
      );
    switch (response.statusCode) {
      case 200:
        _apiResponse.Data = User.fromJson(json.decode(response.body));
        break;
      case 401:
        print((_apiResponse.ApiError as ApiError).error);
        _apiResponse.ApiError = ApiError.fromJson(json.decode(response.body));
        break;
      default:
        print((_apiResponse.ApiError as ApiError).error);
        _apiResponse.ApiError = ApiError.fromJson(json.decode(response.body));
        break;
    }
  } on SocketException {
    _apiResponse.ApiError = ApiError(error: "Server error. Please retry");
  }
  return _apiResponse;
}
```

[More info here](https://oauth.net/2/)

### Firebase Authentication

Firebase Authentication provides backend services & easy-to-use SDKs to authenticate users to your app. It supports authentication using passwords, phone numbers, popular federated identity providers like Google, Facebook and Twitter, and more.

1. Add dependency

Add the `firebase_auth` dependency to your projects pubspec.yaml file:

```dart
dependencies:
  flutter:
    sdk: flutter
  firebase_core: <latest version>
  firebase_auth: <latest version>
```

2. Download dependency

we can download the dependency by just calling one command

```cmd
flutter pub get
```

### Social Authentication

Social authentication is a multi-step authentication flow, allowing you to sign a user into an account or link them with an existing one.

1. Google

To access Google Sign-In, install the official google_sign_in plugin:

```dart
dependencies:
  google_sign_in: <latest version>
```

We have to do some additional configuration for andorid and ios
[More info here](https://pub.dev/packages/google_sign_in)

2. Facebook

Before getting started setup your Facebook Developer App and follow the setup process to enable Facebook Login.

Ensure the "Facebook" sign-in provider is enabled on the Firebase Console. with the Facebook App ID and Secret set.

Install the flutter_facebook_auth plugin:

```dart
dependencies:
  flutter_facebook_auth: <latest version>
```

We have to do some additional configuration for andorid and ios
[More info here](https://pub.dev/packages/flutter_facebook_auth)

3. Apple

Apple announced that any applications using 3rd party login services (such as Facebook, Twitter, Google etc) must also have an Apple Sign-In method. Apple Sign-In is not required for Android devices.

Install the sign_in_with_apple plugin:

```dart
dependencies:
  sign_in_with_apple: <latest version>
```

We don't have to do any additional configuration for andorid and ios
[More info here](https://pub.dev/packages/sign_in_with_apple)

## 14. Authorization

We have role based access mechanism where we are using Stride ID for access management solution. Some modules/functionality access by some users only.

The model is built on a hierarchical relational manner with the Role group forming the top level. Permissions required to perform a certain role (example: vendor, user, subscriber, admin) etc. are grouped under appropriate roles.

<img src="images/role-based-access-control-1024x536-1.jpg" alt="drawing" width="500"/>

There are three ways to handle role based access mechanism

- we may check user access list from authorization module every time

- we may check user access list from authorization module once the user logins

- we may check user access list from authorization module and cache it for 5 mins TTL

## 15. gRPC

In gRPC, a client application can directly call a method on a server application on a different machine as if it were a local object, making it easier for you to create distributed applications and services. As in many RPC systems, gRPC is based around the idea of defining a service, specifying the methods that can be called remotely with their parameters and return types. On the server side, the server implements this interface and runs a gRPC server to handle client calls. On the client side, the client has a stub (referred to as just a client in some languages) that provides the same methods as the server.

<img src="images/grpc_diagram.svg" alt="drawing" width="500"/>

#### Why use gRPC?

With gRPC we can define our service once in a .proto file and generate clients and servers in any of gRPC’s supported languages like dart, which in turn can be run in environments ranging from servers inside a large data center to your own tablet — all the complexity of communication between different languages and environments is handled for you by gRPC. We also get all the advantages of working with protocol buffers, including efficient serialization, a simple IDL, and easy interface updating.

In order to implement gRPC in dart, we will use the package https://pub.dev/packages/grpc. It's a high performance, open source, general RPC framework that puts mobile and HTTP/2 first.

## 16. Exception Handling

Exceptional conditions are things that occur in a system that are not expected or are not a part of normal system operation. When the system handles these exceptional conditions improperly, it can lead to failures and system crashes.

Exception handling is the method of building a system to detect and recover from exceptional conditions. Exceptional conditions are any unexpected occurrences that are not accounted for in a system's normal operation. It is difficult to protect a system from the effects of exceptional conditions because, by nature, all unusual occurrences cannot be anticipated when the system is designed. Some examples of exceptional conditions are incorrect inputs from the user, bit level memory or data corruption, software design defects that cause a system to enter an undefined state, and environmental anomalies. If these exceptional conditions are not properly caught and handled, they can cause an error or failure in the system. Failures due to exceptions are estimated to account for two thirds of system crashes and fifty percent of system security vulnerabilities.

The Flutter framework catches errors that occur during callbacks triggered by the framework itself, including errors encountered during the build, layout, and paint phases. Errors that don’t occur within Flutter’s callbacks can’t be caught by the framework, but you can handle them by setting up a [Zone](https://api.flutter.dev/flutter/dart-async/Zone-class.html).

### Handling all types of errors

```dart
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

void main() {
  if(!kReleaseMode) {
   runZonedGuarded(() async {
      WidgetsFlutterBinding.ensureInitialized();
      await myErrorsHandler.initialize();
      FlutterError.onError = (FlutterErrorDetails details) {
         FlutterError.dumpErrorToConsole(details);
         myErrorsHandler.onError(details);
         exit(1);
      };
      runApp(MyApp());
      }, (Object error, StackTrace stack) {
         myErrorsHandler.onError(error, stack);
         exit(1);
      });
   }else{
      runApp(MyApp());
   }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      builder: (BuildContext context, Widget widget) {
        Widget error = Text('Something went wrong. Please try again later.');
        if (widget is Scaffold || widget is Navigator)
          error = Scaffold(body: Center(child: error));
        ErrorWidget.builder = (FlutterErrorDetails errorDetails) => error;
        return widget;
      },
    );
  }
}
```

**NOTE:** This code snippet handles all errors caught by Flutter framework. if we want to handle exception while calling API or custom places then we have to use `try...catch`.

Dart also provide exception handling mechanism like `try...catch`. Every exception in Dart is a subtype of the pre-defined class `Exception`. Exceptions must be handled to prevent the application from terminating abnormal.

```dart
try {
   // code that might throw an exception
}
on Exception1 {
   // exception handling code
}
catch Exception2 {
   //  exception handling
}
finally {
   // code that should always execute; irrespective of the exception
}
```

[More info here](https://flutter.dev/docs/testing/errors) and [here](https://www.tutorialspoint.com/dart_programming/dart_programming_exceptions.htm)

### Dart async/await exception though catchError

```dart
myFunc().then(processValue)
        .catchError(handleError);
```

Here `then` callback calls automatically when `myFunc` completed successfully and in any case `myFunc` failed to execute then it calls `catchError`.

### Dartz

Dartz is package in dart which allows us to return object in case of success and failure in case of any issue.

```dart
Future<Either<Failure, NumberTrivia>> _getTrivia(
    _ConcreteOrRandomChooser getConcreteOrRandom,
  ) async {
    if (await networkInfo.isConnected) {
      try {
        final remoteTrivia = await getConcreteOrRandom();
        localDataSource.cacheNumberTrivia(remoteTrivia);
        return Right(remoteTrivia);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      try {
        final localTrivia = await localDataSource.getLastNumberTrivia();
        return Right(localTrivia);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }
```

Here if function execution goes well then it will return `Right` otherwise it will return `Left`.

There are some more options as well like `Option` and `Result`. [More info here](https://pub.dev/packages/dartz)

## 17. Caching

Caching is the process of storing copies of files in a cache, or temporary storage location, so that they can be accessed more quickly. Technically, a cache is any temporary storage location for copies of files or data, but the term is often used in reference to Internet technologies.

### Image caching

We can use flutter plugin to cache the network image. So it will cache the image in local storage and it will not fetch the image from network after first time. The name of the plugin is `Cached network image`.

- The CachedNetworkImage can be used directly or through the ImageProvider. Both the CachedNetworkImage as CachedNetworkImageProvider have minimal support for web. It currently doesn't include caching.

```dart
CachedNetworkImage(
  imageUrl: "http://via.placeholder.com/200x150",
  imageBuilder: (context, imageProvider) => Container(
    decoration: BoxDecoration(
      image: DecorationImage(
          image: imageProvider,
          fit: BoxFit.cover,
          colorFilter:
              ColorFilter.mode(Colors.red, BlendMode.colorBurn)),
    ),
  ),
  placeholder: (context, url) => CircularProgressIndicator(),
  errorWidget: (context, url, error) => Icon(Icons.error),
),
```

[More info here](https://pub.dev/packages/cached_network_image)

### Network request caching

We can use `dio_http_cache` for chaching for network request.

#### Add Dependency

```
dependencies:
  dio_http_cache: ^0.2.x #latest version
```

#### QuickStart

1. Add a dio-http-cache interceptor in Dio:

```dart
dio.interceptors.add(DioCacheManager(CacheConfig(baseUrl: "http://www.google.com")).interceptor);
```

2. Set maxAge for a request:

```dart
Dio().get(
"http://www.google.com",
options: buildCacheOptions(Duration(minutes: 30)),
);
```

[More info here](https://pub.dev/packages/dio_http_cache)

## 18. References

[Flutter](https://flutter.dev)

[Dart](https://dart.dev)

[bloclibrary](https://bloclibrary.dev/#/)

[Animation](https://blog.logrocket.com/adding-animations-to-your-flutter-app/)
